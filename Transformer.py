import torch
import torch.nn as nn
import torch.nn.functional as functional


class Head(nn.Module):
    def __init__(self, key_embeds, query_embeds, value_embeds, head_size, block_size, dropout):
        super().__init__()
        self.key = nn.Linear(key_embeds, head_size, bias=False)
        self.query = nn.Linear(query_embeds, head_size, bias=False)
        self.value = nn.Linear(value_embeds, head_size, bias=False)
        self.register_buffer('tril', torch.tril(torch.ones(block_size, block_size)))
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        batch, timestep, channel = x.shape
        k = self.key(x)  # (Batch, Timestep, Channel)
        q = self.query(x)  # (Batch, Timestep, Channel)
        # Compute attention scores ("Affinities")
        weights = q @ k.transpose(-2, -1) * channel ** -0.5  # (B, T, C) @ (B, C, T) => (B, T, T)
        weights = weights.masked_fill(self.tril[:timestep, :timestep] == 0, float('-inf'))
        weights = functional.softmax(weights, dim=-1)  # (B, T, T)
        weights = self.dropout(weights)
        # preform the weighted aggregation of the values
        v = self.value(x)  # (Batch, Timestep, Channel)
        out = weights @ v  # (B, T, T) @ (B, T, C) => (B, T, C)
        return out


class MultiHeadAttention(nn.Module):
    def __init__(self, num_heads, head_size, num_embeddings, block_size, dropout):
        super().__init__()
        self.heads = nn.ModuleList([Head(key_embeds=num_embeddings,
                                         query_embeds=num_embeddings,
                                         value_embeds=num_embeddings,
                                         head_size=head_size,
                                         block_size=block_size,
                                         dropout=dropout) for _ in range(num_heads)])
        self.projection = nn.Linear(num_embeddings, num_embeddings)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        out = torch.cat([h(x) for h in self.heads], dim=-1)
        out = self.projection(out)
        out = self.dropout(out)
        return out


class FeedForward(nn.Module):
    def __init__(self, num_embeddings, dropout):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(num_embeddings, 4 * num_embeddings),
            nn.ReLU(),
            nn.Linear(4 * num_embeddings, num_embeddings),
            nn.Dropout(dropout)
        )

    def forward(self, x):
        return self.net(x)


class Block(nn.Module):
    def __init__(self, num_embeddings, num_heads, block_size, dropout):
        super().__init__()
        head_size = num_embeddings // num_heads
        self.self_attention = MultiHeadAttention(num_heads, head_size, num_embeddings, block_size, dropout)
        self.feedForward = FeedForward(num_embeddings, dropout)
        self.layerNorm1 = nn.LayerNorm(num_embeddings)
        self.layerNorm2 = nn.LayerNorm(num_embeddings)

    def forward(self, x):
        # Adding x for residual connections
        x = x + self.self_attention(self.layerNorm1(x))
        x = x + self.feedForward(self.layerNorm2(x))
        return x


class BigramLanguageModel(nn.Module):
    def __init__(self, vocab_size: int, block_size: int, num_embeddings: int, num_heads: int, num_block_layers: int, dropout: float, device: str) -> None:
        super().__init__()
        self.device = device
        self.block_size = block_size
        # Lookup table to internal embeddings
        self.token_embedding_table = nn.Embedding(vocab_size, num_embeddings)
        self.position_embedding_table = nn.Embedding(block_size, num_embeddings)
        # self.self_attention_heads = MultiHeadAttention(4, num_embeddings//4)
        # self.feedForward = FeedForward(num_embeddings)
        self.blocks = nn.Sequential(
            *[Block(num_embeddings=num_embeddings, num_heads=num_heads, block_size=block_size, dropout=dropout) for _ in
              range(num_block_layers)])
        self.layerNorm = nn.LayerNorm(num_embeddings)
        self.lang_head = nn.Linear(num_embeddings, vocab_size)

    def forward(self, indices, targets=None):
        batch, timestep = indices.shape
        token_embedding = self.token_embedding_table(indices)  # Shape: (Batch, Timestep, Channel)
        position_embedding = self.position_embedding_table(torch.arange(timestep, device=self.device))  # Shape: (Timestep, Channel)
        x = token_embedding + position_embedding  # Shape: (Batch, Timestep, Channel)
        # x = self.self_attention_heads(x) # apply one head of self-attention. Shape: (Batch, Timestep, Channel)
        # x = self.feedForward(x)
        x = self.blocks(x)
        x = self.layerNorm(x)
        logits = self.lang_head(x)  # Shape: (Batch, Timestep, vocab_size)
        if targets is None:
            loss = None
        else:
            batch, timestep, channel = logits.shape
            logits = logits.view(batch * timestep, channel)
            targets = targets.view(batch * timestep)
            loss = functional.cross_entropy(logits, targets)
        return logits, loss

    def generate(self, indices):
        # indices: (Batch, Timestep)
        indices_cond = indices[:, -self.block_size:]
        # Predictions
        logits, _ = self(indices_cond)
        # focus is only on last time step
        logits = logits[:, -1, :]  # Shape: (Batch, Channel)
        # Apply softmax
        probs = functional.softmax(logits, dim=-1)
        # Sample from distribution
        indices_next = torch.multinomial(probs, num_samples=1)  # Shape: (Batch, 1)
        # Append Sampled Index
        return torch.cat((indices, indices_next), dim=1)  # Shape: (Batch, T+1)
