import configparser
import logging
import os.path

import discord
import sentencepiece as spm
import torch
from discord.ext import commands

import Transformer

# from datetime import datetime, time

# configuration
config_path = '.credstore/bot-config.ini'
config = configparser.ConfigParser()
config.read(config_path)

# token used to access discord API
token = ''

# Model hyper parameters
device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Models Path
transformer_path = 'transformer.model'
spm_path = 'spm.model'

try:
    token = config['APP']['TOKEN']
    parameters = config['MODEL']
except:
    logging.info('Failed to open config file ' + config_path + ' ... Quitting.')
    raise

# Model Setup
processor = spm.SentencePieceProcessor()
model = Transformer.BigramLanguageModel(
    vocab_size=int(parameters['VOCAB_SIZE']),
    block_size=int(parameters['BLOCK_SIZE']),
    num_block_layers=int(parameters['NUM_BLOCK_LAYERS']),
    num_heads=int(parameters['NUM_HEADS']),
    num_embeddings=int(parameters['NUM_EMBEDDINGS']),
    dropout=float(parameters['DROPOUT']),
    device=device
)
model = model.to(device)
print('Model Parameters: ', sum(dict((p.data_ptr(), p.numel()) for p in model.parameters()).values()))

# Optimizer
optimizer = torch.optim.AdamW(model.parameters(), lr=float(parameters['LEARNING_RATE']))

# Discord bot setup
intents = discord.Intents.default()
# intents.members = True
intents.message_content = True
bot = commands.Bot(command_prefix='!', intents=intents)


def generate_helper(message: str = None, max_new_tokens: int = 500):
    end = processor.Encode('<END>')
    if message is None:
        context = torch.zeros((1, 1), dtype=torch.int64, device=device)
    else:
        context = torch.tensor([processor.Encode(message + '<END>')], dtype=torch.int64, device=device)
    model.eval()
    with torch.no_grad():
        for _ in range(max_new_tokens):
            context = model.generate(context)
            if context[0].tolist() == end:
                break
    out_message = processor.Decode(context[0].tolist())
    if message is None:
        start_index = 0
    else:
        start_index = out_message.find('<END>') + len('<END>')
    end_index = out_message.find('<END>', start_index)
    out_message = out_message[start_index:end_index]
    return out_message


@bot.event
async def on_connect():
    print('Connected to server!')


@bot.event
async def on_disconnect():
    print('Disconnected from server!')


@bot.event
async def on_ready():
    if os.path.exists(spm_path):
        processor.Init(model_file=spm_path)
    if os.path.exists(transformer_path):
        model.load_state_dict(torch.load(transformer_path))
        model.eval()


@bot.event
async def on_message(message):
    if message.author.bot:
        return
    if bot.user in message.mentions:
        async with message.channel.typing():
            out_message = generate_helper(message.content)
        await message.channel.send(out_message)
    else:
        await bot.process_commands(message)


@bot.command()
async def fetch(context: commands.Context, guild_id: int):
    # Set Parameters
    guild: discord.Guild = bot.get_guild(guild_id)
    if guild is None:
        raise AssertionError('Could not find guild')
    channels: list[discord.TextChannel] = guild.text_channels

    # Fetch Data
    messages = []
    for channel in channels:
        print(f"Fetch data from {channel}")
        messages.extend([message async for message in channel.history(limit=None)])
    messages_content = [message.content for message in messages]
    text = '<END>\n'.join(messages_content).replace('. ', '.\n')
    with open(f'{guild_id}.txt', 'w', encoding="utf-8") as file:
        file.write(text)
    print("Done")


@bot.command()
async def generate(context: commands.Context, message: str = None, max_new_tokens: int = 500):
    async with context.typing():
        out_message = generate_helper(message, max_new_tokens)
    await context.send(out_message)


bot.run(token)
