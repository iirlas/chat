import os
import torch
import time
import Transformer
import configparser
import sentencepiece as spm
import logging

# configuration
config_path = '.credstore/bot-config.ini'
config = configparser.ConfigParser()
config.read(config_path)

try:
    parameters = config['MODEL']
except:
    logging.info('Failed to open config file ' + config_path + ' ... Quitting.')
    raise

# Model hyper parameters
epoch = int(parameters['EPOCH'])
eval_interval = int(parameters['EVAL_INTERVAL'])
eval_iters = int(parameters['EVAL_ITERS'])
device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Models Path
transformer_path = 'transformer.model'
spm_path = 'spm.model'
file_path = config['SPM']['TEXT_PATH']

torch.cuda.empty_cache()
# Model Setup
processor = spm.SentencePieceProcessor()
model = Transformer.BigramLanguageModel(
    vocab_size=int(parameters['VOCAB_SIZE']),
    block_size=int(parameters['BLOCK_SIZE']),
    num_block_layers=int(parameters['NUM_BLOCK_LAYERS']),
    num_heads=int(parameters['NUM_HEADS']),
    num_embeddings=int(parameters['NUM_EMBEDDINGS']),
    dropout=float(parameters['DROPOUT']),
    device=device
)
model = model.to(device)
if os.path.exists(transformer_path):
    model.load_state_dict(torch.load(transformer_path))
    model.eval()
print('Model Parameters: ', sum(dict((p.data_ptr(), p.numel()) for p in model.parameters()).values()))

# Optimizer
optimizer = torch.optim.AdamW(model.parameters(), lr=float(parameters['LEARNING_RATE']))

with open(file_path, 'r', encoding='utf-8') as file:
    messages_content = file.readlines()
    text = ' '.join(messages_content)
if not os.path.exists(spm_path):
    spm.SentencePieceTrainer.Train(
        sentence_iterator=iter(messages_content),
        model_prefix=os.path.splitext(spm_path)[0],
        vocab_size=int(parameters['VOCAB_SIZE']),
        user_defined_symbols='<END>',
        max_sentence_length=int(config['SPM']['MAX_SENTENCE_LENGTH']),
        model_type='unigram'
    )
processor.Init(model_file=spm_path)

# Encode Text
data = torch.tensor(processor.Encode(text), dtype=torch.int64)

# Split data into train/test
split_ratio = float(parameters['SPLIT_RATIO'])
split_index = int(split_ratio * len(data))
train_data = data[:split_index]
test_data = data[split_index:]

def get_batch(split):
    data = train_data if split == 'train' else test_data
    block_size = int(parameters['BLOCK_SIZE'])
    batch_size = int(parameters['BATCH_SIZE'])
    indices = torch.randint(len(data) - block_size, (batch_size,))
    x = torch.stack([data[i:i + block_size] for i in indices])
    y = torch.stack([data[i + 1:i + block_size + 1] for i in indices])
    x, y = x.to(device), y.to(device)
    return x, y

@torch.no_grad()
def average_loss():
    out = {}
    model.eval()
    for split in ['train', 'test']:
        losses = torch.zeros(eval_iters)
        for k in range(eval_iters):
            x_batch, y_batch = get_batch(split)
            _, loss = model(x_batch, y_batch)
            losses[k] = loss.item()
        out[split] = losses.mean()
    model.train()
    return out

 # Training
start_time = time.perf_counter()
for steps in range(1, epoch + 1):
    # Sample Data
    x_batch, y_batch = get_batch('train')
    # Evaluate loss
    logits, loss = model(x_batch, y_batch)
    optimizer.zero_grad(set_to_none=True)
    loss.backward()
    optimizer.step()
    # Print loss
    if steps % eval_interval == 0:
        losses = average_loss()
        print(
            f"{steps}/{epoch} - "
            f"train loss: {losses['train']:.4f}, "
            f"test loss: {losses['test']:.4f} "
            f"[{time.perf_counter() - start_time:0.4f} seconds]"
        )
        start_time = time.perf_counter()
torch.save(model.state_dict(), transformer_path)
